/**
 * Created by fathimtiaz on 2/14/2017.
 */
$('#form-modal').on("shown.bs.modal", function (e) {
    $("#editModalLabel").html($(e.relatedTarget).data('title'));
    $.ajax({
        url:$(e.relatedTarget).data('url'),
        success: function (data) {
            $('#form-modal .modal-body').html(data);
        },
        async: true
    });
});
$('#delete-modal').on("shown.bs.modal", function (e) {
    $("#deleteModalLabel").html($(e.relatedTarget).data('title'));
    $("#data").html($(e.relatedTarget).data('row'));
    $('#action').attr('href', $(e.relatedTarget).data('url'));
});