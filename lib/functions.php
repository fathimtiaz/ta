<?php

function fn_euclidean_distance($x, $y)
{
    $distance = 0;
    for($i=0;$i<count($x);$i++)
    {
        $distance += pow($x[$i] - $y[$i], 2);
    }

    return sqrt($distance);
}

function fn_transpose($arr)
{
    $out = array();
    foreach ($arr as $key => $subarr) {
        foreach ($subarr as $subkey => $subvalue) {
            $out[$subkey][$key] = $subvalue;
        }
    }

    return $out;
}