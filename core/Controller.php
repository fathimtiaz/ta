<?php

class Controller
{
    protected $model;

	public function view($fileName, $data = null)
	{
		$view = new View($fileName, $data);
		echo $view;
	}
}