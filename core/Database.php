<?php

class Database extends \PDO
{

    public function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS)
    {
        parent::__construct($DB_TYPE . ':host=' . $DB_HOST . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASS);
    }

    public function index($table, $array = array(), $fetch = PDO::FETCH_ASSOC)
    {
        $data = 'SELECT * FROM ' . $table;
        $statement = $this->prepare($data);

        foreach ($array as $key => $value) {
            $statement->bindValue("$key", $value);
        }

        $statement->execute();
        return $statement->fetchAll($fetch);
    }

    public function select($data, $array = array(), $fetch = PDO::FETCH_ASSOC) {
        $statement = $this->prepare($data);

        foreach ($array as $key => $value) {
            $statement->bindValue("$key", $value);
        }

        $statement->execute();
        return $statement->fetchAll($fetch);
    }

    public function first($table, $id, $array = array(), $fetch = PDO::FETCH_ASSOC)
    {
        $data = 'SELECT * FROM ' . $table . ' WHERE id = ' . $id;
        $statement = $this->prepare($data);

        foreach ($array as $key => $value) {
            $statement->bindValue("$key", $value);
        }

        $statement->execute();
        return $statement->fetch($fetch);
    }

    public function insert($table, $data)
    {
        ksort($data);

        $fieldNames = implode('`, `', array_keys($data));
        $fieldValues = ':' . implode(', :', array_keys($data));

        $sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        return $sth->execute();
    }

    public function last($table, $array = array(), $fetch = PDO::FETCH_ASSOC)
    {
        $data = 'SELECT * FROM ' . $table . ' ORDER BY id DESC LIMIT 1 ';
        $statement = $this->prepare($data);

        foreach ($array as $key => $value) {
            $statement->bindValue("$key", $value);
        }

        $statement->execute();
        return $statement->fetch($fetch);
    }

    public function update($table, $data, $where)
    {
        ksort($data);

        $fieldDetails = NULL;
        foreach($data as $key=> $value) {
            $fieldDetails .= "`$key`=:$key,";
        }
        $fieldDetails = rtrim($fieldDetails, ',');

        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        return $sth->execute();
    }

    public function delete($table, $where)
    {
        return $this->exec("DELETE FROM $table WHERE $where");
    }
}