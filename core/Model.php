<?php

class Model {

    public $table;

    public $query;

    public $lastInsertId;

    public function __construct() {
        $this->database = new Database(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
    }

    public function index()
    {
        $this->query = $this->database->index($this->table);
    }

    public function indexWith($related_tables)
    {
        $this->query = $this->database->index($this->table);
        foreach($this->query as $index => $query)
        {
            foreach($related_tables as $related_table)
            {
                $class = "\\App\\Models\\" . ucfirst($related_table) . "Model";
                $related_model = new $class;
                $related_model->first($query[$related_table . '_id']);
                $this->query[$index][$related_table] = $related_model->query;
                unset($this->query[$index][$related_table . '_id']);
            }
        }
    }

    public function first($id)
    {
        $this->query = $this->database->first($this->table, $id);
    }

    public function firstWith($id, $related_tables)
    {
        $this->query = $this->database->first($this->table, $id);
        $this->populateRelatedTables($this->query, $related_tables, $id);
    }

    protected function populateRelatedTables(&$query, $related_tables, $id)
    {
        foreach($related_tables as $related_table)
        {
            $words = explode('_', $related_table);
            $class_name = '';
            foreach($words as $word)
            {
                $class_name = $class_name . ucfirst($word);
            }
            $class = "\\App\\Models\\" . $class_name . "Model";
            $related_model = new $class;
            if (array_key_exists($related_table . '_id', $query))
            {
                $related_model->first($query[$related_table . '_id']);
                unset($query[$related_table . '_id']);

            }
            else {
                $q = 'SELECT * FROM ' . $related_table . ' WHERE ' . $this->table . '_id = ' . $id;
                $related_model->select($q);
            }
            $query[$related_table] = $related_model->query;
        }
    }

    public function insert($data)
    {
        return $this->database->insert($this->table, $data);
    }

    public function last()
    {
        $this->query = $this->database->last($this->table);
    }

    public function select($query)
    {
        $this->query = $this->database->select($query);
    }

    public function selectWith($query, $related_tables)
    {
        $this->query = $this->database->select($query);
        foreach($this->query as &$row)
        {
            $this->populateRelatedTables($row, $related_tables, $row['id']);
        }
    }

    public function update($data, $where)
    {
        $this->database->update($this->table, $data, $where);
    }

    public function delete($where)
    {
        $this->database->delete($this->table, $where);
    }
}