<?php 

class App
{
	protected $url = null;
	protected $controller = 'index';
	protected $method = 'index';
	protected $params = [];

	protected $controllerPath = '../app/controllers/';
	protected $errorFile = 'Errors.php';

	public function __construct()
	{
		$this->parseUrl();

		if (empty($this->url[0])) {
			$this->loadDefaultController();
			return false;
		}

		$this->loadExistingController();

		if (isset($this->url[1])) {
			$this->setMethod();
			$this->setParams();
			$this->callControllerMethod();
		}
		else $this->controller->index();

	}

	public function parseUrl()
	{
		$url = isset($_GET['url']) ? $_GET['url'] : null;
		$url = rtrim($url, '/');
		$url = filter_var($url, FILTER_SANITIZE_URL);
		$this->url = explode('/', $url);
	}

	public function loadDefaultController()
	{
		$file = $this->controllerPath.$this->controller.'.php';
		require_once $file;
		$this->controller = new Index();
	}

	public function loadExistingController()
	{
		$file = $this->controllerPath.$this->url[0].'.php';

		if (file_exists($file)) {
			require_once $file;
			$this->controller = new $this->url[0];
		}
		else {
			$this->notFound();
		}
		
		unset($this->url[0]);
	}

	public function setMethod()
	{
		$this->method = $this->url[1];
		unset($this->url[1]);
	}

	public function setParams()
	{
	    array_values($this->url);
		$this->params = $this->url;
	}

	public function callControllerMethod()
	{
	    if(method_exists($this->controller, $this->method))
        {
            call_user_func_array([$this->controller, $this->method], $this->params);
        }
        else $this->notFound();
	}

	public function notFound()
	{
		$file = $this->controllerPath.$this->errorFile;
		require_once $file;
		$this->controller = new Errors();
		$this->controller->notFound();
	}
}