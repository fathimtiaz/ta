
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="../../favicon.ico">
      <title>Tugas Akhir | Fath Imtiaz (140810120009)</title>
      <!-- Bootstrap core CSS -->
      <link href="{{ASSET_ROOT}}/css/bootstrap.min.css" rel="stylesheet">
      <link href="{{ASSET_ROOT}}/css/toastr.min.css" rel="stylesheet">
      <link href="{{ASSET_ROOT}}/css/iCheck/square/blue.css" rel="stylesheet">
      <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
  </head>
  <style>
      .navbar-default {
          background-color: #337ab7;
          border-color: #155c85;
      }
      .navbar-default .navbar-brand {
          color: #ecf0f1;
      }
      .navbar-default .navbar-brand:hover,
      .navbar-default .navbar-brand:focus {
          color: #ffffff;
      }
      .navbar-default .navbar-text {
          color: #ecf0f1;
      }
      .navbar-default .navbar-nav > li > a {
          color: #ecf0f1;
      }
      .navbar-default .navbar-nav > li > a:hover,
      .navbar-default .navbar-nav > li > a:focus {
          color: #ffffff;
      }
      .navbar-default .navbar-nav > .active > a,
      .navbar-default .navbar-nav > .active > a:hover,
      .navbar-default .navbar-nav > .active > a:focus {
          color: #ffffff;
          background-color: #155c85;
      }
      .navbar-default .navbar-nav > .open > a,
      .navbar-default .navbar-nav > .open > a:hover,
      .navbar-default .navbar-nav > .open > a:focus {
          color: #ffffff;
          background-color: #155c85;
      }
      .navbar-default .navbar-toggle {
          border-color: #155c85;
      }
      .navbar-default .navbar-toggle:hover,
      .navbar-default .navbar-toggle:focus {
          background-color: #155c85;
      }
      .navbar-default .navbar-toggle .icon-bar {
          background-color: #ecf0f1;
      }
      .navbar-default .navbar-collapse,
      .navbar-default .navbar-form {
          border-color: #ecf0f1;
      }
      .navbar-default .navbar-link {
          color: #ecf0f1;
      }
      .navbar-default .navbar-link:hover {
          color: #ffffff;
      }

      @media (max-width: 767px) {
          .navbar-default .navbar-nav .open .dropdown-menu > li > a {
              color: #ecf0f1;
          }
          .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
          .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
              color: #ffffff;
          }
          .navbar-default .navbar-nav .open .dropdown-menu > .active > a,
          .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover,
          .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
              color: #ffffff;
              background-color: #155c85;
          }
      }
  </style>

  <body style="padding-top: 65px">
    <div class="container">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
              <a class="navbar-brand" href="{{HTTP_ROOT}}/home"><b>Tugas Akhir</b></a>
          </div>
          <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{HTTP_ROOT}}/clustering">Clustering</a></li>
                <li><a href="{{HTTP_ROOT}}/hasil">Hasil</a></li>
                <li><a href="{{HTTP_ROOT}}/dataset">Dataset</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
    <div class="container">
    {% block content %}
      <div class="starter-template">
        <h1>Bootstrap starter template</h1>
        <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
      </div>
    {% endblock %}
    </div>

    <script
            src="https://code.jquery.com/jquery-3.1.1.js"
            integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
            crossorigin="anonymous"></script>
    <script src="{{ASSET_ROOT}}/js/iCheck/icheck.min.js"></script>
    <script src="{{ASSET_ROOT}}/js/toastr.min.js"></script>
    <script src="{{ASSET_ROOT}}/js/bootstrap.min.js"></script>
    {% block script %}

    {% endblock %}
  </body>
</html>
