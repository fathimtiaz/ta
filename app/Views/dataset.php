{% extends 'base.php'%}

{% block content %}
<div class="panel panel-primary">
    <div class="panel-heading">
        <b>Atur Dataset</b>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <button class="btn btn-success"
                    data-target="#form-modal"
                    data-title="Tambah Dataset"
                    data-toggle="modal"
                    data-url="{{HTTP_ROOT}}/dataset/create"
            >
                <span class="glyphicon glyphicon-plus"></span> Tambah
            </button>
        </div>
        <table class="table table-bordered text-center">
            <thead>
            <tr>
                <td>No</td>
                <td>Nama</td>
                <td>File</td>
                <td></td>
            </tr>
            </thead>
            {% for item in data %}
            <tr>
                <td class="td-fit">{{loop.index}}</td>
                <td>{{item.nama}}</td>
                <td>{{item.nama_file}}</td>
                <td class="td-fit">
                    <button
                            class="btn btn-primary"
                            data-id="{{item.id}}"
                            data-url="{{HTTP_ROOT}}/dataset/edit/{{item.id}}"
                            data-title="Ubah Dataset"
                            data-toggle="modal"
                            data-target="#form-modal"
                    >
                        <span class="glyphicon glyphicon-pencil"></span> Ubah
                    </button>
                    <button
                            class="btn btn-danger"
                            data-url="{{HTTP_ROOT}}/dataset/delete/{{item.id}}"
                            data-row="{{item.nama}}"
                            data-title="Hapus Dataset"
                            data-toggle="modal"
                            data-target="#delete-modal"
                    >
                        <span class="glyphicon glyphicon-trash"></span> Hapus
                    </button>
                </td>
            </tr>
            {% endfor %}
        </table>
    </div>
</div>

{% include 'modal/edit.php' %}

{% include 'modal/delete.php' %}

<style>
    .td-fit{
        white-space: nowrap;
        width:1%;
    }
</style>
{% endblock %}

{% block script %}
<script src="{{ASSET_ROOT}}/js/app/modal.js">
    $('#update-form').on('submit', function (e) {
        alert('asd');
        e.preventDefault();
        $.ajax({
            url: '{{HTTP_ROOT}}/dataset/update/{{data.id}}',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (data) {
                console.log(data);
            },
            error: function (e) {
                console.log(e);
            }
        });

        return false;
    });
    $(document).ready(function(){

    });
</script>
{% endblock %}