{% extends 'base.php'%}

{% block content %}
<div class="panel panel-primary">
    <div class="panel-heading">
        <b>Hasil Clustering</b>
    </div>
    <div class="panel-body" style="height: 250px; overflow: scroll;overflow-x: hidden;">
        <table class="table table-bordered text-center" id="hasil">
            <thead>
                <tr>
                    <td>No</td>
                    <td>Tanggal</td>
                    <td>Dataset</td>
                    <td>Algoritma</td>
                    <td></td>
                </tr>
            </thead>
            {% for item in data %}
            <tr id="baris-{{item.id}}">
                <td class="td-fit">{{loop.index}}</td>
                <td>{{item.tanggal_waktu}}</td>
                <td>{{item.dataset.nama}}</td>
                <td>{{item.algoritma.nama}}</td>
                <td class="td-fit">
                    <button
                        class="btn btn-success"
                        onclick="pilih({{item.id}})"
                    >
                        <span class="glyphicon glyphicon-check"></span> Pilih
                    </button>
                    <button
                        class="btn btn-primary"
                        data-id="{{item.id}}"
                        data-url="{{HTTP_ROOT}}/hasil/edit/{{item.id}}"
                        data-action="{{HTTP_ROOT}}/hasil/update/{{item.id}}"
                        data-title="Ubah Hasil Clustering ({{item.dataset.nama}} - {{item.algoritma.nama}})"
                        data-toggle="modal"
                        data-target="#form-modal"
                    >
                        <span class="glyphicon glyphicon-pencil"></span> Ubah
                    </button>
                    <button
                        class="btn btn-danger"
                        data-url="{{HTTP_ROOT}}/hasil/delete/{{item.id}}"
                        data-row="{{item.dataset.nama}} - {{item.algoritma.nama}}"
                        data-title="Hapus Hasil"
                        data-toggle="modal"
                        data-target="#delete-modal"
                    >
                        <span class="glyphicon glyphicon-trash"></span> Hapus
                    </button>
                </td>
            </tr>
            {% endfor %}
        </table>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <b>Data dan Visualisasi</b>
    </div>
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#keterangan">Keterangan</a></li>
            <li><a data-toggle="tab" href="#scatter">Scatter</a></li>
        </ul>

        <div class="tab-content">
            <div id="keterangan" class="tab-pane fade in active">
                <table class="table" id="keterangan">
                    <tr>
                        <td class="td-fit">Dataset</td>
                        <td class="td-fit">:</td>
                        <td><input class="no-border" type="text" id="nama-dataset"></td>
                        <td class="td-fit">k</td>
                        <td class="td-fit">:</td>
                        <td><input class="no-border" type="text" id="k" name="k"</td>
                    </tr>
                    <tr>
                        <td class="td-fit">Data</td>
                        <td class="td-fit">:</td>
                        <td><input class="no-border" type="text" id="jumlah-data"></td>
                        <td class="td-fit">Iterasi</td>
                        <td class="td-fit">:</td>
                        <td><input class="no-border" type="text" id="iterasi" name="iterasi"></td>
                    </tr>
                    <tr>
                        <td class="td-fit">Data berlabel</td>
                        <td class="td-fit">:</td>
                        <td><input class="no-border" type="text" id="jumlah-data-berlabel"></td>
                        <td class="td-fit">algoritma</td>
                        <td class="td-fit">:</td>
                        <td><input class="no-border" type="text" id="nama-algoritma"></td>
                    </tr>
                    <tr>
                        <td class="td-fit">Cluster</td>
                        <td class="td-fit">:</td>
                        <td class="td-fit" id="pembagian-cluster"></td>
                        <td class="td-fit">Evaluasi</td>
                        <td class="td-fit">:</td>
                        <td class="td-fit" id="evaluasi"></td>
                    </tr>
                </table>
            </div>
            <div id="scatter" class="tab-pane fade" style="padding-top: 10px">
                <div class="col-lg-3">
                    <form class="form-horizontal" id="plot">
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="x">X</label>
                            <div class="col-lg-10">
                                <select id="x" name="x" class="form-control">
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="y">Y</label>
                            <div class="col-lg-10">
                                <select id="y" name="y" class="form-control">
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group pull-right" style="padding-right: 15px">
                            <button type="submit" class="btn btn-primary">plot</button>
                            <button class="btn btn-default">reset</button>
                        </div>
                        <input type="hidden" id="id" name="id">
                    </form>
                </div>
                <div class="col-lg-9">
                    <div id="scatter-div" align="center"></div>
                </div>
            </div>
        </div>
    </div>
</div>

{% include 'modal/edit.php' %}

{% include 'modal/delete.php' %}

<style>
    .td-fit{
        white-space: nowrap;
        width:1%;
    }
    .no-border{
         border: none;
    }
    tr.selected {
        background-color: #f1f1f1;
    }
</style>
{% endblock %}
{% block script %}
<script src="{{ASSET_ROOT}}/js/app/modal.js"></script>
<script>


    $('#hasil tr').on('click', function() {
        $('#hasil tr').removeClass('selected');
        $(this).toggleClass('selected');
    });
    function pilih(id){
        $.ajax({
            url: '{{HTTP_ROOT}}/hasil/show/'+id,
            type:'GET',
            dataType: 'json',
            success: function(data){
                $('#id').val(data['id']);
                $('#x').empty();
                $('#y').empty();
                $.each(data['kolom'], function(index, column){
                    $('#x').append('<option value="'+index+'">'+column+'</option>');
                    $('#y').append('<option value="'+index+'">'+column+'</option>');
                });

                $('#jumlah-data').val(data['dataset']['jumlah_data']);
                $('#jumlah-data-berlabel').val(data['dataset']['jumlah_data_berlabel']);
                $('#nama-dataset').val(data['dataset']['nama']);
                $('#k').val(data['dataset']['k']);
                $('#nama-algoritma').val(data['algoritma']['nama']);
                $('#iterasi').val(data['iterasi']);

                $('#keterangan').prop('hidden', false);

                var clusters_count = data['pembagian_cluster'];
                $('#pembagian-cluster').html('');
                for (var cluster in clusters_count)
                {
                    if(clusters_count.hasOwnProperty(cluster))
                    {
                        $('#pembagian-cluster').append(
                            "Cluster "+cluster+" = " +
                            "<input type='text' name='cluster-"+cluster+"' value='"+clusters_count[cluster]['jumlah']+"' class='no-border'><br>"
                        )
                    }
                }

                var evaluasi = data['clustering_evaluasi'];
                $('#evaluasi').html('');
                for (var value in evaluasi)
                {
                    if(evaluasi.hasOwnProperty(value))
                    {
                        $('#evaluasi').append(
                            evaluasi[value]['evaluasi']['nama'] + " "+value+" = " +
                            "<input type='text' name='evaluasi-"+value+"' value='"+evaluasi[value]['nilai']+"' class='no-border'><br>"
                        )
                    }
                }
            },
            error: function(e){
                toastr.error('');
            }
        });
    }

    $('#plot').on('submit', function(e){
        e.preventDefault();

        var id = $('#id').val();;
        var x = $('#x').val();
        var y = $('#y').val();

        $.ajax({
            url: '{{HTTP_ROOT}}/hasil/plot/'+id+'/'+x+'/'+y,
            type:'GET',
            dataType: 'json',
            success: function(data){
                var trace = new Array();
                for(var i = 0; i < Object.keys(data).length; i++)
                {
                    var data_x = $.map(data[i]['x'], function(value) {
                        return [value];
                    });
                    var data_y = $.map(data[i]['y'], function(value) {
                        return [value];
                    });
                    trace[i] = {
                        x: data_x,
                        y: data_y,
                        name: "cluster " + i,
                        mode: "markers"
                    };
                }
                var layout = {
                    margin: {
                        l: 30,
                        r: 20,
                        b: 20,
                        t: 20,
                        pad: 3
                    },
                    showlegend: true,
                    width: 800,
                    height: 500
                };
                Plotly.newPlot('scatter-div', trace, layout);
            },
            error: function(e){
                toastr.error('');
            }
        })
    })
</script>
{% endblock %}