<form class="form form-horizontal" method="post" action="{{HTTP_ROOT}}/dataset/{{action}}" enctype="multipart/form-data">
    <div class="form-group">
        <label class="control-label col-lg-4">File</label>
        <div class="col-lg-8">
            <input type="file" class="form-control" id="file" name="file" style="color: transparent"
            onchange="uploadOnChange(this)">
            <span id="nama-file">{{data.nama_file}}</span>
            <input type="hidden" name="nama_file_lama" value="{{data.nama_file}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-4">Nama</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" name="nama" value="{{data.nama}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-4">Jumlah data</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" name="jumlah_data" value="{{data.jumlah_data}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-4">Jumlah data berlabel</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" name="jumlah_data_berlabel" value="{{data.jumlah_data_berlabel}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-4">K</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" name="k" value="{{data.k}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-4">Kolom label</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" name="kolom_kelas" value="{{data.kolom_kelas}}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-4">Kolom display</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" name="kolom_display" value="{{data.kolom_display}}">
        </div>
    </div>
    <div class="modal-footer">
        <div class="form-group pull-right">
            <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</form>

<script>
    function uploadOnChange(e) {
        var filename = e.value;
        var lastIndex = filename.lastIndexOf("\\");
        if (lastIndex >= 0) {
            filename = filename.substring(lastIndex + 1);
        }
        document.getElementById('nama-file').innerHTML = filename;
    }
</script>