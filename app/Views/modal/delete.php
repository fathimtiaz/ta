<div class="modal fade" id="delete-modal"
     tabindex="-1" role="dialog"
     aria-labelledby="deleteModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="deleteModalLabel"></h4>
            </div>
            <div class="modal-body">
                <b><span id="data"></span></b>
            </div>
            <div class="modal-footer">
                <div class="form-group pull-right">
                    <a id="action" href="">
                        <button type="button" class="btn btn-danger">Hapus</button>
                    </a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</div>