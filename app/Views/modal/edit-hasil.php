<form class="form form-horizontal" action="{{HTTP_ROOT}}/hasil/update/{{data.id}}" id="update-form">
    {% for item in data.clustering_evaluasi %}
    <div class="form-group">
        <label class="control-label col-lg-5">{{item.evaluasi.nama}}</label>
        <div class="col-lg-7">
            <input type="text" class="form-control"
                   name="{{item.evaluasi.id}}"
                   value="{{item.nilai}}">
        </div>
    </div>
    {% endfor %}
    <div class="modal-footer">
        <div class="form-group pull-right">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</form>
<script>
    $('#update-form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            success: function (data) {
                toastr.success('Data berhasil diubah');
            },
            error: function (e) {
                toastr.error('');
            }
        });

        return false;
    });
</script>