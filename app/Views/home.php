{% extends 'base.php'%}

{% block content %}
<div class="panel panel-primary">
    <div class="panel-heading">
        <b>Rekap Evaluasi Hasil Clustering</b>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <h4 class="text-center">Constrained K-Means</h4>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td>No</td>
                        <td>Dataset</td>
                        <td>Purity</td>
                        <td>Rand</td>
                        <td>DB</td>
                    </tr>
                    </thead>
                    {% for item in constrained %}
                    <tr>
                        <td>{{loop.index}}</td>
                        <td>{{item.dataset.nama}}</td>
                        {% for evaluasi in item.clustering_evaluasi %}
                        <td>{{evaluasi.nilai}}</td>
                        {% endfor %}
                    </tr>
                    {% endfor %}
                </table>
            </div>
            <div class="col-lg-6">
                <h4 class="text-center">Seeded K-Means</h4>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td>No</td>
                        <td>Dataset</td>
                        <td>Purity</td>
                        <td>Rand</td>
                        <td>DB</td>
                    </tr>
                    </thead>
                    {% for item in seeded %}
                    <tr>
                        <td>{{loop.index}}</td>
                        <td>{{item.dataset.nama}}</td>
                        {% for evaluasi in item.clustering_evaluasi %}
                        <td>{{evaluasi.nilai}}</td>
                        {% endfor %}
                    </tr>
                    {% endfor %}
                </table>
            </div>
        </div>
    </div>
</div>
{% endblock %}
{% block script %}
<script>
    var trace1 = {
        x: [1, 2, 3, 4],
        y: [10, 15, 13, 17],
        mode: 'markers'
    };

    var trace2 = {
        x: [2, 3, 4, 5],
        y: [16, 5, 11, 10],
        mode: 'lines'
    };

    var trace3 = {
        x: [1, 2, 3, 4],
        y: [12, 9, 15, 12],
        mode: 'lines+markers'
    };

    var data = [ trace1, trace2, trace3 ];

    var layout = {
        title:'Line and Scatter Plot',
        height: 700,
        width: 700
    };

    Plotly.newPlot('scatter-div', data, layout);
</script>
{% endblock %}