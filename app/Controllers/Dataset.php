<?php

use App\Models\DatasetModel;

class Dataset extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $dataset = new DatasetModel;
        $dataset->index();
        $dataset = $dataset->query;
        $this->view('dataset', ['data' => $dataset]);
    }

    public function edit($id)
    {
        $dataset = new DatasetModel();
        $dataset->first($id);
        $dataset = $dataset->query;
        $this->view('modal/edit-dataset', [
            'data' => $dataset,
            'action' => 'update/' . $dataset['id']
        ]
        );
    }

    public function create()
    {
        $this->view('modal/edit-dataset', ['action' => 'store']);
    }

    public function store()
    {
        $post = [
            'post' => $_POST,
            'files' => $_FILES
        ];
        $dataset = new DatasetModel();
        $dataset->insert($post);
//        var_dump($dataset->insert($post));
//        echo $post['post']['nama'];
//        return false;

        header("Location: " . HTTP_ROOT . "/dataset");
    }

    public function update($id)
    {
        $post = [
            'post' => $_POST,
            'files' => $_FILES
        ];

        $dataset = new DatasetModel();
        $dataset->update($post, $id);

        header("Location: " . HTTP_ROOT . "/dataset");
    }

    public function delete($id)
    {
        $dataset = new DatasetModel();
        $dataset->first($id);
        unlink(INC_ROOT . '/public/datasets/input/' . $dataset->query['nama_file']);
        $dataset->delete("id = $id");

        header("Location: " . HTTP_ROOT . "/dataset");
    }
}