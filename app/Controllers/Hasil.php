<?php

class Hasil extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $clustering = new \App\Models\ClusteringModel();
        $clustering->indexWith(['algoritma', 'dataset']);
        $data = $clustering->query;
        $this->view('hasil', [
            'data' => $data
        ]);
    }

    public function show($id)
    {
        $clustering = new \App\Models\ClusteringModel();
        $clustering->firstWith($id, ['algoritma', 'dataset', 'pembagian_cluster', 'clustering_evaluasi']);
        foreach($clustering->query['clustering_evaluasi'] as $index => &$evaluasi)
        {
            $evaluasi_new = new \App\Models\EvaluasiModel();
            $evaluasi_new->first($evaluasi['evaluasi_id']);
            $evaluasi['evaluasi'] = $evaluasi_new->query;
            unset($evaluasi['evaluasi_id']);
        }

        $data = $clustering->query;
        $data['kolom'] = $clustering->getColumnFromOutputFile();

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function edit($id)
    {
        $clustering = new \App\Models\ClusteringModel();
        $clustering->firstWith($id, ['algoritma', 'dataset', 'clustering_evaluasi']);
        foreach($clustering->query['clustering_evaluasi'] as $index => &$evaluasi)
        {
            $evaluasi_new = new \App\Models\EvaluasiModel();
            $evaluasi_new->first($evaluasi['evaluasi_id']);
            $evaluasi['evaluasi'] = $evaluasi_new->query;
            unset($evaluasi['evaluasi_id']);
        }
        $data = $clustering->query;
        $this->view('modal/edit-hasil', ['data' => $data]);
    }

    public function update($id)
    {
        foreach($_POST as $evaluasi_id => $value)
        {
            $data['nilai'] = $value;
            $clustering_evaluasi = new \App\Models\ClusteringEvaluasiModel();
            $clustering_evaluasi->update($data, "clustering_id = $id AND evaluasi_id = $evaluasi_id");
        }

        header('Content-Type: application/json');
        echo(json_encode(''));
    }

    public function delete($id)
    {
        $path = INC_ROOT."/public/datasets/output/$id";
        array_map('unlink', glob("$path/*.*"));
        rmdir($path);

        $clustering_evaluasi = new \App\Models\ClusteringEvaluasiModel();
        $clustering_evaluasi->delete("clustering_id = $id");

        $pembagian_cluster = new \App\Models\PembagianClusterModel();
        $pembagian_cluster->delete("clustering_id = $id");

        $clustering = new \App\Models\ClusteringModel();
        $clustering->delete("id = $id");

        header("Location: " . HTTP_ROOT . "/hasil");
    }

    public function plot($id, $x, $y)
    {
        $clustering = new \App\Models\ClusteringModel();
        $clustering->firstWith($id, ['dataset']);
        $plot_data = $clustering->getDataForPlot($x, $y);

        header('Content-Type: application/json');
        echo json_encode($plot_data);
    }
}