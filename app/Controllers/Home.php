<?php

Class Home extends Controller
{
	public function __construct()
	{

	}

	public function index()
	{
	    $clustering_constrained = new \App\Models\ClusteringModel();
	    $query = "SELECT * FROM $clustering_constrained->table WHERE algoritma_id = 1";
	    $clustering_constrained->selectWith($query, ['dataset', 'clustering_evaluasi']);

        $clustering_seeded = new \App\Models\ClusteringModel();
        $query = "SELECT * FROM $clustering_seeded->table WHERE algoritma_id = 2";
        $clustering_seeded->selectWith($query, ['dataset', 'clustering_evaluasi']);

		$this->view('home', [
		    'constrained' => $clustering_constrained->query,
            'seeded' => $clustering_seeded->query
        ]);
	}
}