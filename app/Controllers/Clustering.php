<?php

use App\Clusterer\SeededKMeans;
use App\Clusterer\ConstrainedKMeans;
use App\Models;

Class Clustering extends Controller
{
	public function __construct()
	{

	}

	public function index()
	{
	    $model = new Models\DatasetModel;
	    $model->index();
	    $datasets = $model->query;
        $model = new Models\EvaluasiModel;
        $model->index();
        $evaluasi = $model->query;
	    $this->view('clustering', ['datasets' => $datasets, 'evaluasi' => $evaluasi]);
	}

	public function download()
    {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="temp_output.csv";');

        readfile(INC_ROOT.'/public/datasets/output/temp_output.csv', 'w');

    }

	public function start()
    {
        $dataset = new Models\DatasetModel;
        $dataset->clustering($_POST['dataset_id']);

        $algoritma = new Models\AlgoritmaModel;
        $algoritma->first($_POST['algoritma_id']);

        ini_set('max_execution_time', 900);

        $clustering = new Models\ClusteringModel;
        $clustering->setClusters($dataset, $algoritma);

        $dataset->restoreIgnoredColumns();
        $clustering->writeTempOutputFile($dataset);

        header('Content-Type: application/json');
        echo json_encode([
            'clustering' => $clustering,
            'dataset' => $dataset->query,
            'algoritma' => $algoritma
        ]);
    }

    public function save()
    {
        $dataset = new Models\DatasetModel;
        $dataset->clustering($_POST['dataset_id']);

        header('Content-Type: application/json');

        try{
            $clustering = new Models\ClusteringModel();
            $clustering->store($_POST);
            $dataset->restoreIgnoredColumns();
            $clustering->writeOutputFile($dataset, json_decode($_POST['clusters_assignments']));
            $message = 'Data berhasil disimpan';
            echo json_encode([
                'results' => [
                    'message' => $message,
                ]
            ]);
        }
        catch (\Exception $e)
        {
            echo json_encode([
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            ]);
        }
    }

    public function stop()
    {
        exec('Taskkill /PID ' . getmypid() .' /F');
    }
}