<?php

/* modal/edit-hasil.php */
class __TwigTemplate_c2b811d6eb15a6e142f06650784d79659e131e14953c84a24610e907fcc23dad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form class=\"form form-horizontal\" action=\"";
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/hasil/update/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["data"] ?? null), "id", array()), "html", null, true);
        echo "\" id=\"update-form\">
    ";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["data"] ?? null), "clustering_evaluasi", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 3
            echo "    <div class=\"form-group\">
        <label class=\"control-label col-lg-5\">";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "evaluasi", array()), "nama", array()), "html", null, true);
            echo "</label>
        <div class=\"col-lg-7\">
            <input type=\"text\" class=\"form-control\"
                   name=\"";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "evaluasi", array()), "id", array()), "html", null, true);
            echo "\"
                   value=\"";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "nilai", array()), "html", null, true);
            echo "\">
        </div>
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    <div class=\"modal-footer\">
        <div class=\"form-group pull-right\">
            <button type=\"submit\" class=\"btn btn-primary\">Simpan</button>
            <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Tutup</button>
        </div>
    </div>
</form>
<script>
    \$('#update-form').on('submit', function (e) {
        e.preventDefault();
        \$.ajax({
            url: \$(this).attr('action'),
            type: 'POST',
            data: \$(this).serialize(),
            dataType: 'json',
            success: function (data) {
                toastr.success('Data berhasil diubah');
            },
            error: function (e) {
                toastr.error('');
            }
        });

        return false;
    });
</script>";
    }

    public function getTemplateName()
    {
        return "modal/edit-hasil.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 12,  43 => 8,  39 => 7,  33 => 4,  30 => 3,  26 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modal/edit-hasil.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\modal\\edit-hasil.php");
    }
}
