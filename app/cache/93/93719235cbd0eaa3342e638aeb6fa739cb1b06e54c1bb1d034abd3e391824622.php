<?php

/* base.php */
class __TwigTemplate_55e218b7ca905a7fac37ccf7c3dd1fdd9a4ef528fabba588b87f5c8e767762d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!DOCTYPE html>
<html lang=\"en\">
  <head>
      <meta charset=\"utf-8\">
      <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name=\"description\" content=\"\">
      <meta name=\"author\" content=\"\">
      <link rel=\"icon\" href=\"../../favicon.ico\">
      <title>Tugas Akhir | Fath Imtiaz (140810120009)</title>
      <!-- Bootstrap core CSS -->
      <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, ($context["ASSET_ROOT"] ?? null), "html", null, true);
        echo "/css/bootstrap.min.css\" rel=\"stylesheet\">
      <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, ($context["ASSET_ROOT"] ?? null), "html", null, true);
        echo "/css/toastr.min.css\" rel=\"stylesheet\">
      <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, ($context["ASSET_ROOT"] ?? null), "html", null, true);
        echo "/css/iCheck/square/blue.css\" rel=\"stylesheet\">
      <script src=\"https://cdn.plot.ly/plotly-latest.min.js\"></script>
  </head>
  <style>
      .navbar-default {
          background-color: #337ab7;
          border-color: #155c85;
      }
      .navbar-default .navbar-brand {
          color: #ecf0f1;
      }
      .navbar-default .navbar-brand:hover,
      .navbar-default .navbar-brand:focus {
          color: #ffffff;
      }
      .navbar-default .navbar-text {
          color: #ecf0f1;
      }
      .navbar-default .navbar-nav > li > a {
          color: #ecf0f1;
      }
      .navbar-default .navbar-nav > li > a:hover,
      .navbar-default .navbar-nav > li > a:focus {
          color: #ffffff;
      }
      .navbar-default .navbar-nav > .active > a,
      .navbar-default .navbar-nav > .active > a:hover,
      .navbar-default .navbar-nav > .active > a:focus {
          color: #ffffff;
          background-color: #155c85;
      }
      .navbar-default .navbar-nav > .open > a,
      .navbar-default .navbar-nav > .open > a:hover,
      .navbar-default .navbar-nav > .open > a:focus {
          color: #ffffff;
          background-color: #155c85;
      }
      .navbar-default .navbar-toggle {
          border-color: #155c85;
      }
      .navbar-default .navbar-toggle:hover,
      .navbar-default .navbar-toggle:focus {
          background-color: #155c85;
      }
      .navbar-default .navbar-toggle .icon-bar {
          background-color: #ecf0f1;
      }
      .navbar-default .navbar-collapse,
      .navbar-default .navbar-form {
          border-color: #ecf0f1;
      }
      .navbar-default .navbar-link {
          color: #ecf0f1;
      }
      .navbar-default .navbar-link:hover {
          color: #ffffff;
      }

      @media (max-width: 767px) {
          .navbar-default .navbar-nav .open .dropdown-menu > li > a {
              color: #ecf0f1;
          }
          .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
          .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
              color: #ffffff;
          }
          .navbar-default .navbar-nav .open .dropdown-menu > .active > a,
          .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover,
          .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
              color: #ffffff;
              background-color: #155c85;
          }
      }
  </style>

  <body style=\"padding-top: 65px\">
    <div class=\"container\">
      <nav class=\"navbar navbar-default navbar-fixed-top\">
        <div class=\"container\">
          <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
              <span class=\"sr-only\">Toggle navigation</span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
            </button>
              <a class=\"navbar-brand\" href=\"";
        // line 102
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/home\"><b>Tugas Akhir</b></a>
          </div>
          <div id=\"navbar\" class=\"collapse navbar-collapse\">
            <ul class=\"nav navbar-nav\">
                <li><a href=\"";
        // line 106
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/clustering\">Clustering</a></li>
                <li><a href=\"";
        // line 107
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/hasil\">Hasil</a></li>
                <li><a href=\"";
        // line 108
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/dataset\">Dataset</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
    <div class=\"container\">
    ";
        // line 115
        $this->displayBlock('content', $context, $blocks);
        // line 121
        echo "    </div>

    <script
            src=\"https://code.jquery.com/jquery-3.1.1.js\"
            integrity=\"sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=\"
            crossorigin=\"anonymous\"></script>
    <script src=\"";
        // line 127
        echo twig_escape_filter($this->env, ($context["ASSET_ROOT"] ?? null), "html", null, true);
        echo "/js/iCheck/icheck.min.js\"></script>
    <script src=\"";
        // line 128
        echo twig_escape_filter($this->env, ($context["ASSET_ROOT"] ?? null), "html", null, true);
        echo "/js/toastr.min.js\"></script>
    <script src=\"";
        // line 129
        echo twig_escape_filter($this->env, ($context["ASSET_ROOT"] ?? null), "html", null, true);
        echo "/js/bootstrap.min.js\"></script>
    ";
        // line 130
        $this->displayBlock('script', $context, $blocks);
        // line 133
        echo "  </body>
</html>
";
    }

    // line 115
    public function block_content($context, array $blocks = array())
    {
        // line 116
        echo "      <div class=\"starter-template\">
        <h1>Bootstrap starter template</h1>
        <p class=\"lead\">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
      </div>
    ";
    }

    // line 130
    public function block_script($context, array $blocks = array())
    {
        // line 131
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "base.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 131,  199 => 130,  191 => 116,  188 => 115,  182 => 133,  180 => 130,  176 => 129,  172 => 128,  168 => 127,  160 => 121,  158 => 115,  148 => 108,  144 => 107,  140 => 106,  133 => 102,  44 => 16,  40 => 15,  36 => 14,  21 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\base.php");
    }
}
