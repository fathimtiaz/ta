<?php

/* modal/edit.php */
class __TwigTemplate_9af3fbab9b800ef45fad759bb4c5acabb3a0032873fb266c46c9b67e4f54068b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"form-modal\"
     tabindex=\"-1\" role=\"dialog\"
     aria-labelledby=\"editModalLabel\">
    <div class=\"modal-dialog modal-md\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\"
                        data-dismiss=\"modal\"
                        aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\"
                    id=\"editModalLabel\"></h4>
            </div>
            <div class=\"modal-body\">
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modal/edit.php";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modal/edit.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\modal\\edit.php");
    }
}
