<?php

/* clustering.php */
class __TwigTemplate_694328514556b9da26d50391915e68b6c665f1dc14e8b91588d3c063867e10c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.php", "clustering.php", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.php";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div id=\"loader\"></div>
<div id=\"konten\">
    <form class=\"form-horizontal\" id=\"form-mulai\">
        <div class=\"col-md-4 kiri\">
            <div class=\"panel panel-primary\">
                <div class=\"panel-heading\">
                    <b>Algoritma</b>
                </div>
                <div class=\"panel-body\">
                    <div class=\"form-group\">
                        <div class=\"col-sm-10\">
                            <input name=\"algoritma_id\" value=\"1\" type=\"radio\">
                            <label>Constrained-K-Means</label>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"col-sm-10\">
                            <input name=\"algoritma_id\" value=\"2\" type=\"radio\">
                            <label>Seeded-K-Means</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"panel panel-primary\">
                <div class=\"panel-heading\">
                    <b>Dataset</b>
                </div>
                <div class=\"panel-body\">
                    <div class=\"well dataset-list\">
                        <ul class=\"list-group\" style=\"height:119px;\">
                            ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["datasets"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["dataset"]) {
            // line 35
            echo "                            <li class=\"list-group-item\">
                                <input type=\"radio\" name=\"dataset_id\" value=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["dataset"], "id", array()), "html", null, true);
            echo "\">
                                ";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["dataset"], "nama", array()), "html", null, true);
            echo "
                            </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dataset'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "                        </ul>
                    </div>
                </div>
            </div>
            <div class=\"panel panel-primary text-center\">
                <div class=\"panel-body\">
                    <button class=\"btn btn-primary\" id=\"mulai-btn\">Mulai</button>
                    <button class=\"btn btn-danger\" id=\"berhenti-btn\" disabled>Berhenti</button>
                    <button class=\"btn btn-success\" id=\"save-hasil-btn\" disabled>Simpan</button>
                    <button class=\"btn btn-default\" id=\"reset-btn\">Reset</button>
                </div>
            </div>
        </div>
    </form>
    <div class=\"col-md-8 kanan\">
        <div class=\"panel panel-primary\">
            <div class=\"panel-heading\">
                <b>Hasil</b>
            </div>
            <div class=\"panel-body result\" id=\"result\" style=\"height: 350px;\">
                <form name=\"save-hasil-form\">
                    <table class=\"table\" id=\"tabel-hasil\" hidden>
                        <tr>
                            <td class=\"td-fit\">Data</td>
                            <td class=\"td-fit\">:</td>
                            <td><input class=\"save-hasil\" type=\"text\" id=\"jumlah-data\"></td>
                        </tr>
                        <tr>
                            <td class=\"td-fit\">Data berlabel</td>
                            <td class=\"td-fit\">:</td>
                            <td><input class=\"save-hasil\" type=\"text\" id=\"jumlah-data-berlabel\"></td>
                        </tr>
                        <tr>
                            <td class=\"td-fit\">Algoritma</td>
                            <td class=\"td-fit\">:</td>
                            <td><input class=\"save-hasil\" type=\"text\" id=\"nama-algoritma\"></td>
                        </tr>
                        <tr>
                            <td class=\"td-fit\">k</td>
                            <td class=\"td-fit\">:</td>
                            <td><input class=\"save-hasil\" type=\"text\" id=\"k\" name=\"k\"</td>
                        </tr>
                        <tr>
                            <td class=\"td-fit\">Iterasi</td>
                            <td class=\"td-fit\">:</td>
                            <td><input class=\"save-hasil\" type=\"text\" id=\"iterasi\" name=\"iterasi\"></td>
                        </tr>
                        <tr>
                            <td class=\"td-fit\">Cluster</td>
                            <td class=\"td-fit\">:</td>
                            <td id=\"pembagian-cluster\"></td>
                        </tr>
                    </table>
                    <input type=\"hidden\" id=\"dataset\" name=\"dataset\">
                    <input type=\"hidden\" id=\"dataset_id\" name=\"dataset_id\">
                    <input type=\"hidden\" id=\"algoritma_id\" name=\"algoritma_id\">
                    <input type=\"hidden\" id=\"clusters_assignments\" name=\"clusters_assignments\">
                    <input type=\"hidden\" id=\"clusters_count\" name=\"clusters_count\">
                </form>
            </div>
        </div>
        <div class=\"panel panel-primary\">
            <div class=\"panel-heading\">
                <b>Evaluasi</b>
            </div>
            <div class=\"panel-body\" style=\"height: 76px;\">
                <form class=\"form-horizontal text-right\" name=\"save-hasil-form\">
                    ";
        // line 107
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["evaluasi"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 108
            echo "                    <div class=\"form-group col-md-4\">
                        <label class=\"col-md-6 control-label\">";
            // line 109
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "nama", array()), "html", null, true);
            echo "</label>
                        <div class=\"col-sm-6\" style=\"margin-left: 0; padding-left: 0; margin-right: 0; padding-right: 0\">
                            <input type=\"text\" class=\"form-control evaluasi\" name=\"";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "nama", array()), "html", null, true);
            echo "\" disabled required>
                        </div>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .save-hasil{
        border: none;
    }
    .td-fit{
        white-space: nowrap;
        width:1%;
    }

    .dataset-list .list-group{
        overflow: scroll;
        overflow-x: hidden;
    }

    .result{
        overflow: scroll;
        overflow-x: hidden;
    }

    .kiri {
        padding-left: 0;
    }

    .kanan{
        padding-right: 0;
    }

    #loader {
        position: absolute;
        visibility: hidden;
        left: 50%;
        top: 50%;
        z-index: 1;
        width: 150px;
        height: 150px;
        margin: -75px 0 0 -75px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
";
    }

    // line 177
    public function block_script($context, array $blocks = array())
    {
        // line 178
        echo "<script>
    \$('#mulai-btn').on('click', function(e){
        \$('#berhenti-btn').prop('disabled', false);
        e.preventDefault();
        document.getElementById('loader').style.visibility= \"visible\";
        document.getElementById('konten').style.opacity= \"0.4\";
        \$.ajax({
            url: '";
        // line 185
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/clustering/start',
            type:'POST',
            data: \$('#form-mulai').serialize(),
            dataType: 'json',
            success: function(data){
                document.getElementById('loader').style.visibility= \"hidden\";
                document.getElementById('konten').style.opacity= \"1\";

                var clustering = data['clustering'];
                var dataset = data['dataset'];
                var algoritma = data['algoritma'];

                document.location.href = '";
        // line 197
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/clustering/download';

                \$('#save-hasil-btn').prop('disabled', false);
                \$('.evaluasi').prop('disabled', false);

                \$('#jumlah-data').val(dataset['jumlah_data']);
                \$('#jumlah-data-berlabel').val(dataset['jumlah_data_berlabel']);
                \$('#k').val(dataset['k']);
                \$('#nama-algoritma').val(algoritma['query']['nama']);
                \$('#iterasi').val(clustering['iteration']);
                \$('#algoritma_id').val(algoritma['query']['id']);
                \$('#dataset_id').val(dataset['id']);
                \$('#clusters_assignments').val(JSON.stringify(clustering['clustersAssignments']));
                \$('#clusters_count').val(JSON.stringify(clustering['clustersCounts']));

                \$('#tabel-hasil').prop('hidden', false);

                var clusters_count = clustering['clustersCounts'];
                \$('#pembagian-cluster').html('');
                for (var cluster in clusters_count)
                {
                    if(clusters_count.hasOwnProperty(cluster))
                    {
                        \$('#pembagian-cluster').append(
                            \"Cluster \"+cluster+\" = \" +
                            \"<input type='text' name='cluster-\"+cluster+\"' value='\"+clusters_count[cluster]+\"' class='save-hasil'><br>\"
                    )
                    }
                }
                console.log(data);
            },
            error: function(e){
                document.getElementById('loader').style.visibility= \"hidden\";
                document.getElementById('konten').style.opacity= \"1\";
                console.log(e.responseText);
                console.log(e);
                if(e.status !== 0)
                    toastr.error('Dataset dan/atau algoritma belum dipilih');
            }
        });
    });

    function validateForm() {
        var x = document.forms[\"save-hasil-form\"][\"rand\"].value;
        if (x == null) {
            alert(\"Name must be filled out\");
            return false;
        }
    }

    \$('#save-hasil-btn').on('click', function(e){
        document.getElementById('loader').style.visibility= \"visible\";
        document.getElementById('konten').style.opacity= \"0.4\";
        e.preventDefault();
        \$.ajax({
            url: '";
        // line 252
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/clustering/save',
            type: 'POST',
            data: \$(\"[name='save-hasil-form']\").serialize(),
            dataType: 'json',
            success: function(data){
                document.getElementById('loader').style.visibility= \"hidden\";
                document.getElementById('konten').style.opacity= \"1\";
                console.log(data);
                if(data.error)
                {
                    toastr.error(data.error.message);
                }
                else toastr.success(data.results.message);
            },
            error: function(e) {
                document.getElementById('loader').style.visibility= \"hidden\";
                document.getElementById('konten').style.opacity= \"1\";
                console.log(e);
                toastr.error(\"Proses gagal\");
            }
        });
    });

    \$('#reset-btn').on('click', function(e){
        e.preventDefault();
        document.getElementById('loader').style.visibility= \"hidden\";
        \$('input[name=algoritma_id]').iCheck('uncheck');
        \$('input[name=dataset_id]').iCheck('uncheck');
        \$('#save-hasil-btn').prop('disabled', true);
        \$('.evaluasi').prop('disabled', true);
        \$('#tabel-hasil').prop('hidden', true);
    });

    \$('#berhenti-btn').on('click', function(e){
        document.getElementById('loader').style.visibility= \"hidden\";
        document.getElementById('konten').style.opacity= \"1\";
        \$(this).prop('disabled', true);
        e.preventDefault();
        \$.ajax({
            url: '";
        // line 291
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/clustering/stop',
            type: 'POST',
            dataType: 'json',
            success: function(){
            },
            error: function(e) {
                console.log(e);
                toastr.success('Proses dihentikan');
            }
        });
    });
</script>

<script>
    \$(document).ready(function(){
        \$('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "clustering.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  366 => 291,  324 => 252,  266 => 197,  251 => 185,  242 => 178,  239 => 177,  175 => 115,  165 => 111,  160 => 109,  157 => 108,  153 => 107,  84 => 40,  75 => 37,  71 => 36,  68 => 35,  64 => 34,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "clustering.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\clustering.php");
    }
}
