<?php

/* modal/delete.php */
class __TwigTemplate_c1564da363baa51db11ecab515694ead7fd7015c0d91622b494faa48a9a92bc9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"delete-modal\"
     tabindex=\"-1\" role=\"dialog\"
     aria-labelledby=\"deleteModalLabel\">
    <div class=\"modal-dialog modal-sm\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\"
                        data-dismiss=\"modal\"
                        aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\"
                    id=\"deleteModalLabel\"></h4>
            </div>
            <div class=\"modal-body\">
                <b><span id=\"data\"></span></b>
            </div>
            <div class=\"modal-footer\">
                <div class=\"form-group pull-right\">
                    <a id=\"action\" href=\"\">
                        <button type=\"button\" class=\"btn btn-danger\">Hapus</button>
                    </a>
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modal/delete.php";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modal/delete.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\modal\\delete.php");
    }
}
