<?php

/* home.php */
class __TwigTemplate_dbeb921709e1640ffffa5545cfcef98f2adf106a9d01505b1399f2ce9b7f70e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.php", "home.php", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.php";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"panel panel-primary\">
    <div class=\"panel-heading\">
        <b>Evaluasi Hasil Clustering</b>
    </div>
    <div class=\"panel-body\">
        <ul class=\"nav nav-tabs\">
            <li class=\"active\"><a data-toggle=\"tab\" href=\"#tabel\">Tabel</a></li>
            <li><a data-toggle=\"tab\" href=\"#grafik\">Grafik</a></li>
        </ul>
        <div class=\"tab-content\">
            <div id=\"tabel\" class=\"tab-pane fade in active\">
                <div class=\"row\">
                    <div class=\"col-lg-6\">
                        <h4 class=\"text-center\">Constrained-K-Means</h4>
                        <table class=\"table table-bordered\">
                            <thead>
                            <tr>
                                <td>Dataset</td>
                                <td>Jaccard</td>
                                <td>Rand</td>
                                <td>D. Bouldin</td>
                            </tr>
                            </thead>
                            ";
        // line 27
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["constrained"]) ? $context["constrained"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 28
            echo "                            <tr>
                                <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "dataset", array()), "nama_display", array()), "html", null, true);
            echo "</td>
                                ";
            // line 30
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "clustering_evaluasi", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["evaluasi"]) {
                // line 31
                echo "                                    <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["evaluasi"], "nilai", array()), "html", null, true);
                echo "</td>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evaluasi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "                            </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                        </table>
                    </div>
                    <div class=\"col-lg-6\">
                        <h4 class=\"text-center\">Seeded-K-Means</h4>
                        <table class=\"table table-bordered\">
                            <thead>
                            <tr>
                                <td>Dataset</td>
                                <td>Jaccard</td>
                                <td>Rand</td>
                                <td>D. Bouldin</td>
                            </tr>
                            </thead>
                            ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["seeded"]) ? $context["seeded"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 49
            echo "                            <tr>
                                <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "dataset", array()), "nama_display", array()), "html", null, true);
            echo "</td>
                                ";
            // line 51
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "clustering_evaluasi", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["evaluasi"]) {
                // line 52
                echo "                                <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["evaluasi"], "nilai", array()), "html", null, true);
                echo "</td>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evaluasi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "                            </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "                        </table>
                    </div>
                </div>
            </div>
            <div id=\"grafik\" class=\"tab-pane fade\">
                <div id=\"scatter-div\" style=\"width: 700px; height: 700px;\"></div>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 67
    public function block_script($context, array $blocks = array())
    {
        // line 68
        echo "<script>
    var trace1 = {
        x: [1, 2, 3, 4],
        y: [10, 15, 13, 17],
        mode: 'markers'
    };

    var trace2 = {
        x: [2, 3, 4, 5],
        y: [16, 5, 11, 10],
        mode: 'lines'
    };

    var trace3 = {
        x: [1, 2, 3, 4],
        y: [12, 9, 15, 12],
        mode: 'lines+markers'
    };

    var data = [ trace1, trace2, trace3 ];

    var layout = {
        title:'Line and Scatter Plot',
        height: 700,
        width: 700
    };

    Plotly.newPlot('scatter-div', data, layout);
</script>
";
    }

    public function getTemplateName()
    {
        return "home.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 68,  148 => 67,  134 => 56,  127 => 54,  118 => 52,  114 => 51,  110 => 50,  107 => 49,  103 => 48,  88 => 35,  81 => 33,  72 => 31,  68 => 30,  64 => 29,  61 => 28,  57 => 27,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "home.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\home.php");
    }
}
