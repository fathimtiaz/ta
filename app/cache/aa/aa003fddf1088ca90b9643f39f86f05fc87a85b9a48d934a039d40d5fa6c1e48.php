<?php

/* dataset.php */
class __TwigTemplate_94ea6f415e4bcd21157e73d37b5bfcd1c8d255421d47d5cbd1d84ddac0dd0d2c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.php", "dataset.php", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.php";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"panel panel-primary\">
    <div class=\"panel-heading\">
        <b>Atur Dataset</b>
    </div>
    <div class=\"panel-body\">
        <div class=\"form-group\">
            <button class=\"btn btn-success\"
                    data-target=\"#form-modal\"
                    data-title=\"Tambah Dataset\"
                    data-toggle=\"modal\"
                    data-url=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["HTTP_ROOT"]) ? $context["HTTP_ROOT"] : null), "html", null, true);
        echo "/dataset/create\"
            >
                <span class=\"glyphicon glyphicon-plus\"></span> Tambah
            </button>
        </div>
        <table class=\"table table-bordered text-center\">
            <thead>
            <tr>
                <td>ID</td>
                <td>Nama</td>
                <td>File</td>
                <td></td>
            </tr>
            </thead>
            ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 29
            echo "            <tr>
                <td class=\"td-fit\">";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "nama_display", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "nama_file", array()), "html", null, true);
            echo "</td>
                <td class=\"td-fit\">
                    <button
                            class=\"btn btn-primary\"
                            data-id=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                            data-url=\"";
            // line 37
            echo twig_escape_filter($this->env, (isset($context["HTTP_ROOT"]) ? $context["HTTP_ROOT"] : null), "html", null, true);
            echo "/dataset/edit/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                            data-title=\"Ubah Dataset (";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "nama_display", array()), "html", null, true);
            echo ")\"
                            data-toggle=\"modal\"
                            data-target=\"#form-modal\"
                    >
                        <span class=\"glyphicon glyphicon-pencil\"></span> Ubah
                    </button>
                    <button
                            class=\"btn btn-danger\"
                            data-url=\"";
            // line 46
            echo twig_escape_filter($this->env, (isset($context["HTTP_ROOT"]) ? $context["HTTP_ROOT"] : null), "html", null, true);
            echo "/dataset/delete/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                            data-row=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "nama_display", array()), "html", null, true);
            echo "\"
                            data-title=\"Hapus Dataset\"
                            data-toggle=\"modal\"
                            data-target=\"#delete-modal\"
                    >
                        <span class=\"glyphicon glyphicon-trash\"></span> Hapus
                    </button>
                </td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "        </table>
    </div>
</div>

";
        // line 61
        $this->loadTemplate("modal/edit.php", "dataset.php", 61)->display($context);
        // line 62
        echo "
";
        // line 63
        $this->loadTemplate("modal/delete.php", "dataset.php", 63)->display($context);
        // line 64
        echo "
<style>
    .td-fit{
        white-space: nowrap;
        width:1%;
    }
</style>
";
    }

    // line 73
    public function block_script($context, array $blocks = array())
    {
        // line 74
        echo "<script src=\"";
        echo twig_escape_filter($this->env, (isset($context["ASSET_ROOT"]) ? $context["ASSET_ROOT"] : null), "html", null, true);
        echo "/js/app/modal.js\">
    \$('#update-form').on('submit', function (e) {
        alert('asd');
        e.preventDefault();
        \$.ajax({
            url: '";
        // line 79
        echo twig_escape_filter($this->env, (isset($context["HTTP_ROOT"]) ? $context["HTTP_ROOT"] : null), "html", null, true);
        echo "/dataset/update/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "id", array()), "html", null, true);
        echo "',
            type: 'POST',
            data: \$(this).serialize(),
            dataType: 'json',
            success: function (data) {
                console.log(data);
            },
            error: function (e) {
                console.log(e);
            }
        });

        return false;
    });
    \$(document).ready(function(){

    });
</script>
";
    }

    public function getTemplateName()
    {
        return "dataset.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 79,  153 => 74,  150 => 73,  139 => 64,  137 => 63,  134 => 62,  132 => 61,  126 => 57,  110 => 47,  104 => 46,  93 => 38,  87 => 37,  83 => 36,  76 => 32,  72 => 31,  68 => 30,  65 => 29,  61 => 28,  44 => 14,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "dataset.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\dataset.php");
    }
}
