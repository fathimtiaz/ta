<?php

/* hasil.php */
class __TwigTemplate_b4163accc7de747312fc788ee25ee916b7bf057f2717a372ba39d137e5e5c7a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.php", "hasil.php", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.php";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"panel panel-primary\">
    <div class=\"panel-heading\">
        <b>Hasil Clustering</b>
    </div>
    <div class=\"panel-body\" style=\"height: 250px; overflow: scroll;overflow-x: hidden;\">
        <table class=\"table table-bordered text-center\" id=\"hasil\">
            <thead>
                <tr>
                    <td>No</td>
                    <td>Tanggal</td>
                    <td>Dataset</td>
                    <td>Algoritma</td>
                    <td></td>
                </tr>
            </thead>
            ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["data"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 20
            echo "            <tr id=\"baris-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\">
                <td class=\"td-fit\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "tanggal_waktu", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "dataset", array()), "nama", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "algoritma", array()), "nama", array()), "html", null, true);
            echo "</td>
                <td class=\"td-fit\">
                    <button
                        class=\"btn btn-success\"
                        onclick=\"pilih(";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo ")\"
                    >
                        <span class=\"glyphicon glyphicon-check\"></span> Pilih
                    </button>
                    <button
                        class=\"btn btn-primary\"
                        data-id=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                        data-url=\"";
            // line 35
            echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
            echo "/hasil/edit/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                        data-action=\"";
            // line 36
            echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
            echo "/hasil/update/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                        data-title=\"Ubah Hasil Clustering (";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "dataset", array()), "nama", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "algoritma", array()), "nama", array()), "html", null, true);
            echo ")\"
                        data-toggle=\"modal\"
                        data-target=\"#form-modal\"
                    >
                        <span class=\"glyphicon glyphicon-pencil\"></span> Ubah
                    </button>
                    <button
                        class=\"btn btn-danger\"
                        data-url=\"";
            // line 45
            echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
            echo "/hasil/delete/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                        data-row=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "dataset", array()), "nama", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "algoritma", array()), "nama", array()), "html", null, true);
            echo "\"
                        data-title=\"Hapus Hasil\"
                        data-toggle=\"modal\"
                        data-target=\"#delete-modal\"
                    >
                        <span class=\"glyphicon glyphicon-trash\"></span> Hapus
                    </button>
                </td>
            </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "        </table>
    </div>
</div>
<div class=\"panel panel-primary\">
    <div class=\"panel-heading\">
        <b>Data dan Visualisasi</b>
    </div>
    <div class=\"panel-body\">
        <ul class=\"nav nav-tabs\">
            <li class=\"active\"><a data-toggle=\"tab\" href=\"#keterangan\">Keterangan</a></li>
            <li><a data-toggle=\"tab\" href=\"#scatter\">Scatter</a></li>
        </ul>

        <div class=\"tab-content\">
            <div id=\"keterangan\" class=\"tab-pane fade in active\">
                <table class=\"table\" id=\"keterangan\">
                    <tr>
                        <td class=\"td-fit\">Dataset</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"nama-dataset\"></td>
                        <td class=\"td-fit\">k</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"k\" name=\"k\"</td>
                    </tr>
                    <tr>
                        <td class=\"td-fit\">Data</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"jumlah-data\"></td>
                        <td class=\"td-fit\">Iterasi</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"iterasi\" name=\"iterasi\"></td>
                    </tr>
                    <tr>
                        <td class=\"td-fit\">Data berlabel</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"jumlah-data-berlabel\"></td>
                        <td class=\"td-fit\">algoritma</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"nama-algoritma\"></td>
                    </tr>
                    <tr>
                        <td class=\"td-fit\">Cluster</td>
                        <td class=\"td-fit\">:</td>
                        <td class=\"td-fit\" id=\"pembagian-cluster\"></td>
                        <td class=\"td-fit\">Evaluasi</td>
                        <td class=\"td-fit\">:</td>
                        <td class=\"td-fit\" id=\"evaluasi\"></td>
                    </tr>
                </table>
            </div>
            <div id=\"scatter\" class=\"tab-pane fade\" style=\"padding-top: 10px\">
                <div class=\"col-lg-3\">
                    <form class=\"form-horizontal\" id=\"plot\">
                        <div class=\"form-group\">
                            <label class=\"col-lg-2 control-label\" for=\"x\">X</label>
                            <div class=\"col-lg-10\">
                                <select id=\"x\" name=\"x\" class=\"form-control\">
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"col-lg-2 control-label\" for=\"y\">Y</label>
                            <div class=\"col-lg-10\">
                                <select id=\"y\" name=\"y\" class=\"form-control\">
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class=\"form-group pull-right\" style=\"padding-right: 15px\">
                            <button type=\"submit\" class=\"btn btn-primary\">plot</button>
                            <button class=\"btn btn-default\">reset</button>
                        </div>
                        <input type=\"hidden\" id=\"id\" name=\"id\">
                    </form>
                </div>
                <div class=\"col-lg-9\">
                    <div id=\"scatter-div\" align=\"center\"></div>
                </div>
            </div>
        </div>
    </div>
</div>

";
        // line 140
        $this->loadTemplate("modal/edit.php", "hasil.php", 140)->display($context);
        // line 141
        echo "
";
        // line 142
        $this->loadTemplate("modal/delete.php", "hasil.php", 142)->display($context);
        // line 143
        echo "
<style>
    .td-fit{
        white-space: nowrap;
        width:1%;
    }
    .no-border{
         border: none;
    }
    tr.selected {
        background-color: #f1f1f1;
    }
</style>
";
    }

    // line 157
    public function block_script($context, array $blocks = array())
    {
        // line 158
        echo "<script src=\"";
        echo twig_escape_filter($this->env, ($context["ASSET_ROOT"] ?? null), "html", null, true);
        echo "/js/app/modal.js\"></script>
<script>


    \$('#hasil tr').on('click', function() {
        \$('#hasil tr').removeClass('selected');
        \$(this).toggleClass('selected');
    });
    function pilih(id){
        \$.ajax({
            url: '";
        // line 168
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/hasil/show/'+id,
            type:'GET',
            dataType: 'json',
            success: function(data){
                \$('#id').val(data['id']);
                \$('#x').empty();
                \$('#y').empty();
                \$.each(data['kolom'], function(index, column){
                    \$('#x').append('<option value=\"'+index+'\">'+column+'</option>');
                    \$('#y').append('<option value=\"'+index+'\">'+column+'</option>');
                });

                \$('#jumlah-data').val(data['dataset']['jumlah_data']);
                \$('#jumlah-data-berlabel').val(data['dataset']['jumlah_data_berlabel']);
                \$('#nama-dataset').val(data['dataset']['nama']);
                \$('#k').val(data['dataset']['k']);
                \$('#nama-algoritma').val(data['algoritma']['nama']);
                \$('#iterasi').val(data['iterasi']);

                \$('#keterangan').prop('hidden', false);

                var clusters_count = data['pembagian_cluster'];
                \$('#pembagian-cluster').html('');
                for (var cluster in clusters_count)
                {
                    if(clusters_count.hasOwnProperty(cluster))
                    {
                        \$('#pembagian-cluster').append(
                            \"Cluster \"+cluster+\" = \" +
                            \"<input type='text' name='cluster-\"+cluster+\"' value='\"+clusters_count[cluster]['jumlah']+\"' class='no-border'><br>\"
                        )
                    }
                }

                var evaluasi = data['clustering_evaluasi'];
                \$('#evaluasi').html('');
                for (var value in evaluasi)
                {
                    if(evaluasi.hasOwnProperty(value))
                    {
                        \$('#evaluasi').append(
                            evaluasi[value]['evaluasi']['nama'] + \" \"+value+\" = \" +
                            \"<input type='text' name='evaluasi-\"+value+\"' value='\"+evaluasi[value]['nilai']+\"' class='no-border'><br>\"
                        )
                    }
                }
            },
            error: function(e){
                toastr.error('');
            }
        });
    }

    \$('#plot').on('submit', function(e){
        e.preventDefault();

        var id = \$('#id').val();;
        var x = \$('#x').val();
        var y = \$('#y').val();

        \$.ajax({
            url: '";
        // line 229
        echo twig_escape_filter($this->env, ($context["HTTP_ROOT"] ?? null), "html", null, true);
        echo "/hasil/plot/'+id+'/'+x+'/'+y,
            type:'GET',
            dataType: 'json',
            success: function(data){
                var trace = new Array();
                for(var i = 0; i < Object.keys(data).length; i++)
                {
                    var data_x = \$.map(data[i]['x'], function(value) {
                        return [value];
                    });
                    var data_y = \$.map(data[i]['y'], function(value) {
                        return [value];
                    });
                    trace[i] = {
                        x: data_x,
                        y: data_y,
                        name: \"cluster \" + i,
                        mode: \"markers\"
                    };
                }
                var layout = {
                    margin: {
                        l: 30,
                        r: 20,
                        b: 20,
                        t: 20,
                        pad: 3
                    },
                    showlegend: true,
                    width: 800,
                    height: 500
                };
                Plotly.newPlot('scatter-div', trace, layout);
            },
            error: function(e){
                toastr.error('');
            }
        })
    })
</script>
";
    }

    public function getTemplateName()
    {
        return "hasil.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  351 => 229,  287 => 168,  273 => 158,  270 => 157,  253 => 143,  251 => 142,  248 => 141,  246 => 140,  160 => 56,  134 => 46,  128 => 45,  115 => 37,  109 => 36,  103 => 35,  99 => 34,  90 => 28,  83 => 24,  79 => 23,  75 => 22,  71 => 21,  66 => 20,  49 => 19,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "hasil.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\hasil.php");
    }
}
