<?php

/* hasil.php */
class __TwigTemplate_252e7cc88bba03b678367279f5939b14efd83652a94e5b35c1baf5589702e1da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.php", "hasil.php", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.php";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"panel panel-primary\">
    <div class=\"panel-heading\">
        <b>Hasil Clustering</b>
    </div>
    <div class=\"panel-body\" style=\"height: 250px; overflow: scroll;overflow-x: hidden;\">
        <table class=\"table table-bordered text-center\" id=\"hasil\">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Tanggal</td>
                    <td>Dataset</td>
                    <td>Algoritma</td>
                    <td></td>
                </tr>
            </thead>
            ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 20
            echo "            <tr id=\"baris-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\">
                <td class=\"td-fit\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "tanggal_waktu", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "dataset", array()), "nama_display", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "algoritma", array()), "nama_display", array()), "html", null, true);
            echo "</td>
                <td class=\"td-fit\">
                    <button
                        class=\"btn btn-success\"
                        onclick=\"pilih(";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo ")\"
                    >
                        <span class=\"glyphicon glyphicon-check\"></span> Pilih
                    </button>
                    <button
                        class=\"btn btn-primary\"
                        data-id=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                        data-url=\"";
            // line 35
            echo twig_escape_filter($this->env, (isset($context["HTTP_ROOT"]) ? $context["HTTP_ROOT"] : null), "html", null, true);
            echo "/hasil/edit/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                        data-action=\"";
            // line 36
            echo twig_escape_filter($this->env, (isset($context["HTTP_ROOT"]) ? $context["HTTP_ROOT"] : null), "html", null, true);
            echo "/hasil/update/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                        data-title=\"Ubah Hasil Clustering (";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "dataset", array()), "nama_display", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "algoritma", array()), "nama_display", array()), "html", null, true);
            echo ")\"
                        data-toggle=\"modal\"
                        data-target=\"#form-modal\"
                    >
                        <span class=\"glyphicon glyphicon-pencil\"></span> Ubah
                    </button>
                    <button
                        class=\"btn btn-danger\"
                        data-url=\"";
            // line 45
            echo twig_escape_filter($this->env, (isset($context["HTTP_ROOT"]) ? $context["HTTP_ROOT"] : null), "html", null, true);
            echo "/hasil/delete/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", array()), "html", null, true);
            echo "\"
                        data-row=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "dataset", array()), "nama_display", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "algoritma", array()), "nama_display", array()), "html", null, true);
            echo "\"
                        data-title=\"Hapus Hasil\"
                        data-toggle=\"modal\"
                        data-target=\"#delete-modal\"
                    >
                        <span class=\"glyphicon glyphicon-trash\"></span> Hapus
                    </button>
                </td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "        </table>
    </div>
</div>
<div class=\"panel panel-primary\">
    <div class=\"panel-heading\">
        <b>Data dan Visualisasi</b>
    </div>
    <div class=\"panel-body\">
        <ul class=\"nav nav-tabs\">
            <li class=\"active\"><a data-toggle=\"tab\" href=\"#keterangan\">Keterangan</a></li>
            <li><a data-toggle=\"tab\" href=\"#scatter\">Scatter</a></li>
        </ul>

        <div class=\"tab-content\">
            <div id=\"keterangan\" class=\"tab-pane fade in active\">
                <table class=\"table\" id=\"keterangan\">
                    <tr>
                        <td class=\"td-fit\">Data</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"jumlah-data\"></td>
                        <td class=\"td-fit\">k</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"k\" name=\"k\"</td>
                    </tr>
                    <tr>
                        <td class=\"td-fit\">Data berlabel</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"jumlah-data-berlabel\"></td>
                        <td class=\"td-fit\">Iterasi</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"iterasi\" name=\"iterasi\"></td>
                    </tr>
                    <tr>
                        <td class=\"td-fit\">Atribut</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"jumlah-atribut\"></td>
                        <td class=\"td-fit\">algoritma</td>
                        <td class=\"td-fit\">:</td>
                        <td><input class=\"no-border\" type=\"text\" id=\"nama-algoritma\"></td>
                    </tr>
                    <tr>
                        <td class=\"td-fit\">Cluster</td>
                        <td class=\"td-fit\">:</td>
                        <td class=\"td-fit\" id=\"pembagian-cluster\"></td>
                        <td class=\"td-fit\">Evaluasi</td>
                        <td class=\"td-fit\">:</td>
                        <td class=\"td-fit\" id=\"evaluasi\"></td>
                    </tr>
                </table>
            </div>
            <div id=\"scatter\" class=\"tab-pane fade\" style=\"padding-top: 10px\">
                <div class=\"col-lg-3\">
                    <form class=\"form-horizontal\" id=\"plot\">
                        <div class=\"form-group\">
                            <label class=\"col-lg-2 control-label\" for=\"x\">X</label>
                            <div class=\"col-lg-10\">
                                <select id=\"x\" name=\"x\" class=\"form-control\">
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label class=\"col-lg-2 control-label\" for=\"y\">Y</label>
                            <div class=\"col-lg-10\">
                                <select id=\"y\" name=\"y\" class=\"form-control\">
                                    <option>...</option>
                                </select>
                            </div>
                        </div>
                        <div class=\"form-group pull-right\" style=\"padding-right: 15px\">
                            <button type=\"submit\" class=\"btn btn-primary\">plot</button>
                            <button class=\"btn btn-default\">reset</button>
                        </div>
                        <input type=\"hidden\" id=\"id\" name=\"id\">
                    </form>
                </div>
                <div class=\"col-lg-9\">
                    <div id=\"scatter-div\" align=\"center\"></div>
                </div>
            </div>
        </div>
    </div>
</div>

";
        // line 140
        $this->loadTemplate("modal/edit.php", "hasil.php", 140)->display($context);
        // line 141
        echo "
";
        // line 142
        $this->loadTemplate("modal/delete.php", "hasil.php", 142)->display($context);
        // line 143
        echo "
<style>
    .td-fit{
        white-space: nowrap;
        width:1%;
    }
    .no-border{
         border: none;
    }
    tr.selected {
        background-color: #f1f1f1;
    }
</style>
";
    }

    // line 157
    public function block_script($context, array $blocks = array())
    {
        // line 158
        echo "<script src=\"";
        echo twig_escape_filter($this->env, (isset($context["ASSET_ROOT"]) ? $context["ASSET_ROOT"] : null), "html", null, true);
        echo "/js/app/modal.js\"></script>
<script>


    \$('#hasil tr').on('click', function() {
        \$('#hasil tr').removeClass('selected');
        \$(this).toggleClass('selected');
    });
    function pilih(id){
        \$.ajax({
            url: '";
        // line 168
        echo twig_escape_filter($this->env, (isset($context["HTTP_ROOT"]) ? $context["HTTP_ROOT"] : null), "html", null, true);
        echo "/hasil/show/'+id,
            type:'GET',
            dataType: 'json',
            success: function(data){
                \$('#id').val(data['id']);
                \$('#x').empty();
                \$('#y').empty();
                \$.each(data['kolom'], function(index, column){
                    \$('#x').append('<option value=\"'+index+'\">'+column+'</option>');
                    \$('#y').append('<option value=\"'+index+'\">'+column+'</option>');
                });

                \$('#jumlah-data').val(data['dataset']['jumlah_data']);
                \$('#jumlah-data-berlabel').val(data['dataset']['jumlah_data_berlabel']);
                \$('#jumlah-atribut').val(data['dataset']['jumlah_atribut']);
                \$('#k').val(data['dataset']['k']);
                \$('#nama-algoritma').val(data['algoritma']['nama_display']);
                \$('#iterasi').val(data['iterasi']);

                \$('#keterangan').prop('hidden', false);

                var clusters_count = data['pembagian_cluster'];
                \$('#pembagian-cluster').html('');
                for (var cluster in clusters_count)
                {
                    if(clusters_count.hasOwnProperty(cluster))
                    {
                        \$('#pembagian-cluster').append(
                            \"Cluster \"+cluster+\" = \" +
                            \"<input type='text' name='cluster-\"+cluster+\"' value='\"+clusters_count[cluster]['jumlah']+\"' class='no-border'><br>\"
                        )
                    }
                }

                var evaluasi = data['clustering_evaluasi'];
                \$('#evaluasi').html('');
                for (var value in evaluasi)
                {
                    if(evaluasi.hasOwnProperty(value))
                    {
                        \$('#evaluasi').append(
                            evaluasi[value]['evaluasi']['nama_display'] + \" \"+value+\" = \" +
                            \"<input type='text' name='evaluasi-\"+value+\"' value='\"+evaluasi[value]['nilai']+\"' class='no-border'><br>\"
                        )
                    }
                }
            },
            error: function(e){
                toastr.error('');
            }
        });
    }

    \$('#plot').on('submit', function(e){
        e.preventDefault();

        var id = \$('#id').val();;
        var x = \$('#x').val();
        var y = \$('#y').val();

        \$.ajax({
            url: '";
        // line 229
        echo twig_escape_filter($this->env, (isset($context["HTTP_ROOT"]) ? $context["HTTP_ROOT"] : null), "html", null, true);
        echo "/hasil/plot/'+id+'/'+x+'/'+y,
            type:'GET',
            dataType: 'json',
            success: function(data){
                var trace = new Array();
                for(var i = 0; i < Object.keys(data).length; i++)
                {
                    var data_x = \$.map(data[i]['x'], function(value) {
                        return [value];
                    });
                    var data_y = \$.map(data[i]['y'], function(value) {
                        return [value];
                    });
                    trace[i] = {
                        x: data_x,
                        y: data_y,
                        mode: \"markers\"
                    };
                }
                var layout = {
                    margin: {
                        l: 30,
                        r: 20,
                        b: 20,
                        t: 20,
                        pad: 3
                    },
                    showlegend: true,
                    width: 800,
                    height: 500
                };
                Plotly.newPlot('scatter-div', trace, layout);
            },
            error: function(e){
                toastr.error('');
            }
        })
    })
</script>
";
    }

    public function getTemplateName()
    {
        return "hasil.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  330 => 229,  266 => 168,  252 => 158,  249 => 157,  232 => 143,  230 => 142,  227 => 141,  225 => 140,  139 => 56,  121 => 46,  115 => 45,  102 => 37,  96 => 36,  90 => 35,  86 => 34,  77 => 28,  70 => 24,  66 => 23,  62 => 22,  58 => 21,  53 => 20,  49 => 19,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "hasil.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\hasil.php");
    }
}
