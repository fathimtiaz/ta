<?php

/* home.php */
class __TwigTemplate_da6294ace8e5d98b16d169e51892698024905eb82874b06ed3e04398670c9381 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.php", "home.php", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.php";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"panel panel-primary\">
    <div class=\"panel-heading\">
        <b>Rekap Evaluasi Hasil Clustering</b>
    </div>
    <div class=\"panel-body\">
        <div class=\"row\">
            <div class=\"col-lg-6\">
                <h4 class=\"text-center\">Constrained K-Means</h4>
                <table class=\"table table-bordered\">
                    <thead>
                    <tr>
                        <td>No</td>
                        <td>Dataset</td>
                        <td>Purity</td>
                        <td>Rand</td>
                        <td>DB</td>
                    </tr>
                    </thead>
                    ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["constrained"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 23
            echo "                    <tr>
                        <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "dataset", array()), "nama", array()), "html", null, true);
            echo "</td>
                        ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "clustering_evaluasi", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["evaluasi"]) {
                // line 27
                echo "                        <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["evaluasi"], "nilai", array()), "html", null, true);
                echo "</td>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evaluasi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "                    </tr>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "                </table>
            </div>
            <div class=\"col-lg-6\">
                <h4 class=\"text-center\">Seeded K-Means</h4>
                <table class=\"table table-bordered\">
                    <thead>
                    <tr>
                        <td>No</td>
                        <td>Dataset</td>
                        <td>Purity</td>
                        <td>Rand</td>
                        <td>DB</td>
                    </tr>
                    </thead>
                    ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["seeded"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 46
            echo "                    <tr>
                        <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "dataset", array()), "nama", array()), "html", null, true);
            echo "</td>
                        ";
            // line 49
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "clustering_evaluasi", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["evaluasi"]) {
                // line 50
                echo "                        <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["evaluasi"], "nilai", array()), "html", null, true);
                echo "</td>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['evaluasi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "                    </tr>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "                </table>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 60
    public function block_script($context, array $blocks = array())
    {
        // line 61
        echo "<script>
    var trace1 = {
        x: [1, 2, 3, 4],
        y: [10, 15, 13, 17],
        mode: 'markers'
    };

    var trace2 = {
        x: [2, 3, 4, 5],
        y: [16, 5, 11, 10],
        mode: 'lines'
    };

    var trace3 = {
        x: [1, 2, 3, 4],
        y: [12, 9, 15, 12],
        mode: 'lines+markers'
    };

    var data = [ trace1, trace2, trace3 ];

    var layout = {
        title:'Line and Scatter Plot',
        height: 700,
        width: 700
    };

    Plotly.newPlot('scatter-div', data, layout);
</script>
";
    }

    public function getTemplateName()
    {
        return "home.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 61,  189 => 60,  180 => 54,  165 => 52,  156 => 50,  152 => 49,  148 => 48,  144 => 47,  141 => 46,  124 => 45,  108 => 31,  93 => 29,  84 => 27,  80 => 26,  76 => 25,  72 => 24,  69 => 23,  52 => 22,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "home.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\home.php");
    }
}
