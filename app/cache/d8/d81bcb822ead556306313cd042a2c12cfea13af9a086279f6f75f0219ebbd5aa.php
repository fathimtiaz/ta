<?php

/* modal/edit-dataset.php */
class __TwigTemplate_e4cc96b4c23a09a3e59ab5d207707b6d1b6566dcf5f9500e9c79ed4ea4881bef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form class=\"form form-horizontal\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, (isset($context["HTTP_ROOT"]) ? $context["HTTP_ROOT"] : null), "html", null, true);
        echo "/dataset/";
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : null), "html", null, true);
        echo "\" enctype=\"multipart/form-data\">
    <div class=\"form-group\">
        <label class=\"control-label col-lg-4\">File</label>
        <div class=\"col-lg-8\">
            <input type=\"file\" class=\"form-control\" id=\"file\" name=\"file\" style=\"color: transparent\"
            onchange=\"uploadOnChange(this)\">
            <span id=\"nama-file\">";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "nama_file", array()), "html", null, true);
        echo "</span>
            <input type=\"hidden\" name=\"nama_file_lama\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "nama_file", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <label class=\"control-label col-lg-4\">Nama</label>
        <div class=\"col-lg-8\">
            <input type=\"text\" class=\"form-control\" name=\"nama\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "nama", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <label class=\"control-label col-lg-4\">Nama display</label>
        <div class=\"col-lg-8\">
            <input type=\"text\" class=\"form-control\" name=\"nama_display\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "nama_display", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <label class=\"control-label col-lg-4\">Jumlah data</label>
        <div class=\"col-lg-8\">
            <input type=\"text\" class=\"form-control\" name=\"jumlah_data\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jumlah_data", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <label class=\"control-label col-lg-4\">Jumlah data berlabel</label>
        <div class=\"col-lg-8\">
            <input type=\"text\" class=\"form-control\" name=\"jumlah_data_berlabel\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jumlah_data_berlabel", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <label class=\"control-label col-lg-4\">K</label>
        <div class=\"col-lg-8\">
            <input type=\"text\" class=\"form-control\" name=\"k\" value=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "k", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <label class=\"control-label col-lg-4\">Kolom label</label>
        <div class=\"col-lg-8\">
            <input type=\"text\" class=\"form-control\" name=\"kolom_kelas\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "kolom_kelas", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"form-group\">
        <label class=\"control-label col-lg-4\">Kolom display</label>
        <div class=\"col-lg-8\">
            <input type=\"text\" class=\"form-control\" name=\"kolom_display\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "kolom_display", array()), "html", null, true);
        echo "\">
        </div>
    </div>
    <div class=\"modal-footer\">
        <div class=\"form-group pull-right\">
            <button type=\"submit\" class=\"btn btn-primary\" id=\"simpan\">Simpan</button>
            <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Tutup</button>
        </div>
    </div>
</form>

<script>
    function uploadOnChange(e) {
        var filename = e.value;
        var lastIndex = filename.lastIndexOf(\"\\\\\");
        if (lastIndex >= 0) {
            filename = filename.substring(lastIndex + 1);
        }
        document.getElementById('nama-file').innerHTML = filename;
    }
</script>";
    }

    public function getTemplateName()
    {
        return "modal/edit-dataset.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 50,  89 => 44,  80 => 38,  71 => 32,  62 => 26,  53 => 20,  44 => 14,  35 => 8,  31 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modal/edit-dataset.php", "C:\\xampp\\htdocs\\ta\\app\\Views\\modal\\edit-dataset.php");
    }
}
