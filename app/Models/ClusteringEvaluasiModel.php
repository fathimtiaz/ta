<?php
/**
 * Created by PhpStorm.
 * User: fathimtiaz
 * Date: 2/4/2017
 * Time: 9:03 PM
 */

namespace App\Models;


class ClusteringEvaluasiModel extends \Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'clustering_evaluasi';
    }

    public function create($clustering_id, $evaluasi, $post)
    {
        $data['clustering_id'] = $clustering_id;
        $data['evaluasi_id'] = $evaluasi['id'];
        $data['nilai'] = $post[$evaluasi['nama']];

        $this->insert($data);
    }
}