<?php
/**
 * Created by PhpStorm.
 * User: fathimtiaz
 * Date: 2/4/2017
 * Time: 5:59 PM
 */

namespace App\Models;

use App\Models\Clusterer\ConstrainedKMeans;
use App\Models\Clusterer\SeededKMeans;

class ClusteringModel extends \Model
{
    public $clustersAssignments;

    public $clustersCounts;

    public $iteration;

    protected $outputDirectory = "/public/datasets/output/";

    public function __construct()
    {
        parent::__construct();
        $this->table = 'clustering';
    }

    public function setClusters(DatasetModel $dataset, AlgoritmaModel $algoritma)
    {
        switch ($algoritma->query['id'])
        {
            case 1:
                $clusterer = new ConstrainedKMeans($dataset);
                break;
            case 2:
                $clusterer = new SeededKMeans($dataset);
                break;
            default:
                $clusterer = null;
        }

        $this->clustersAssignments = $clusterer->clustersAssignments;
        $this->clustersCounts = $clusterer->clustersCounts;
        $this->iteration = $clusterer->iteration;
    }

    public function store($post)
    {
        $data['algoritma_id'] = $post['algoritma_id'];
        $data['dataset_id'] = $post['dataset_id'];
        $data['iterasi'] = $post['iterasi'];
        $now = new \DateTime('now');
        $data['tanggal_waktu'] = $now->format("Y-m-d H:i:s");

        $evaluasi = new EvaluasiModel();
        $evaluasi->index();
        $evaluasi->validate($evaluasi->query, $post);

        if(!$this->insert($data))
        {
            throw new \Exception('Terdapat clustering dengan dataset dan algoritma yang sama');
        }
        $this->last();
        $last_id = $this->query['id'];

        $this->createPembagianCluster($last_id, $post);
        $this->createClusteringEvaluasi($last_id, $post);
    }

    public function createClusteringEvaluasi($clustering_id, $post)
    {
        $evaluasi = new EvaluasiModel();
        $evaluasi->index();
        foreach($evaluasi->query as $query)
        {
            $clustering_evaluasi = new ClusteringEvaluasiModel();
            $clustering_evaluasi->create($clustering_id, $query, $post);
        }
    }

    public function createPembagianCluster($clustering_id, $post)
    {
        for($i = 0; $i < $post['k']; $i++)
        {
            $cluster = new PembagianClusterModel();
            $cluster->create($i, $post['cluster-'.$i], $clustering_id);
        }
    }

    public function writeTempOutputFile($dataset)
    {
        $data = $this->populateClusterColumn($dataset, $this->clustersAssignments);

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data.csv');

        $path = INC_ROOT . $this->outputDirectory;
        $file = $path . '/temp_output.csv';

        $this->writeToFile($file, $dataset, $data);
    }

    public function writeOutputFile($dataset, $clusters_assignments)
    {
        $data = $this->populateClusterColumn($dataset, $clusters_assignments);

        $directory = $this->database->last($this->table)['id'];
        $path = INC_ROOT . $this->outputDirectory . $directory;
        mkdir($path, 0777, true);
        $file = $path . '/dataset.csv';

        file_put_contents($file, '');
        $this->writeToFile($file, $dataset, $data);
    }

    private function writeToFile($file, $dataset, $data)
    {
        $fp = fopen($file, 'w');

        fputcsv($fp, $dataset->columns);

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }

    private function populateClusterColumn($dataset, $clusters_assignments)
    {
        $data = $dataset->data;
        foreach ($clusters_assignments as $cluster => $instances) {
            foreach ($instances as $row) {
                $data[$row][$dataset->query['kolom_kelas']] = $cluster;
            }
        }
        return $data;
    }

    private function getOutputFile()
    {
        return INC_ROOT . $this->outputDirectory . $this->query['id'] . '/dataset.csv';
    }

    public function getColumnFromOutputFile()
    {
        $data = $this->getDataFromOutputFile();
        return $data[0];
    }

    public function getDataFromOutputFile()
    {
        $file = $this->getOutputFile();
        return array_map('str_getcsv', file($file));
    }

    public function getDataForPlot($x, $y)
    {
        $data = $this->getDataFromOutputFile();
        unset($data[0]);
        array_values($data);

        $plot_data = array();
        foreach($data as $index => $instance)
        {
            for($i = 0; $i < $this->query['dataset']['k']; $i++)
            {
                if($instance[$this->query['dataset']['kolom_kelas']] == $i)
                {
                    $plot_data[$i]['x'][$index] = $instance[$x];
                    $plot_data[$i]['y'][$index] = $instance[$y];
                }
            }
        }
        return $plot_data;
    }
}