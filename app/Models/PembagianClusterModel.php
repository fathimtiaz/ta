<?php
/**
 * Created by PhpStorm.
 * User: fathimtiaz
 * Date: 2/4/2017
 * Time: 9:03 PM
 */

namespace App\Models;

class PembagianClusterModel extends \Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'pembagian_cluster';
    }

    public function create($cluster, $count, $clustering_id)
    {
        $data['cluster'] = $cluster;
        $data['jumlah'] = $count;
        $data['clustering_id'] = $clustering_id;

        $this->insert($data);
    }
}