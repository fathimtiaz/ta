<?php
/**
 * Created by PhpStorm.
 * User: fathimtiaz
 * Date: 2/13/2017
 * Time: 9:57 PM
 */

namespace App\Models;


class EvaluasiModel extends \Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'evaluasi';
    }

    public function validate($query, $post)
    {
        foreach($query as $q)
        {
            $this->validateRequired($post[$q['nama']]);

            if ($q['nama'] != 'DB')
            $this->validateValue($post[$q['nama']]);
        }
    }

    private function validateValue($eval)
    {
        if ($eval < 0 || $eval > 1)
        {
            throw new \Exception('Nilai Purity atau Rand Index tidak valid');
        }
        else
            return true;
    }

    private function validateRequired($post)
    {
        if ($post == null)
            throw new \Exception('Seluruh nilai evaluasi harus diinput');
    }
}