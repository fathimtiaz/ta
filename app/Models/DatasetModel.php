<?php

namespace App\Models;

class DatasetModel extends \Model
{
    public $labeledData;

    public $unlabeledData;

    public $data;

    public $columns;

    public $ignoredData;

    public function __construct()
    {
        parent::__construct();
        $this->table = 'dataset';
    }

    public function clustering($id)
    {
        $this->first($id);
        $this->setData();
        $this->prepareData();
    }

    protected function setData()
    {
        $this->data = array_map('str_getcsv', file(INC_ROOT . '/public/datasets/input/' . $this->query['nama_file']));
    }

    protected function prepareData()
    {
        $this->removeHeaderRow();
        $this->splitDataByLabel();
        $this->removeIgnoredColumns();
    }

    protected function removeHeaderRow()
    {
        $this->columns = $this->data[0];
        unset($this->data[0]);
        $this->data = array_values($this->data);
    }

    protected function getDataCount()
    {
        return count($this->data);
    }

    protected function splitDataByLabel()
    {
        foreach($this->data as $index => $data)
        {
            if($data[$this->query['kolom_kelas']] != null)
                $this->labeledData[$index] = $data;
            else $this->unlabeledData[$index] = $data;
        }
    }

    protected function removeIgnoredColumns()
    {
        foreach($this->data as &$data)
        {
            $this->ignoredData[$this->query['kolom_display']][] = $data[$this->query['kolom_display']];
            $data[$this->query['kolom_display']] = null;
            $data[$this->query['kolom_kelas']] = null;
        }
    }

    public function restoreIgnoredColumns()
    {
        foreach($this->data as $index => &$data)
        {
            $data[$this->query['kolom_display']] = $this->ignoredData[$this->query['kolom_display']][$index];
        }
    }

    public function update($post, $id)
    {
        if(!empty($post['files']['file']['name']))
        {
            $path = INC_ROOT."/public/datasets/input/";
            $old_file = $path . $post['post']['nama_file_lama'];
            unlink($old_file);

            $new_file_name = $post['files']['file']['name'];
            $new_file = $path . $new_file_name;
            move_uploaded_file($post['files']['file']['tmp_name'], $new_file);
            $data['nama_file'] = $new_file_name;
        }
        $data['nama'] = $post['post']['nama'];
        $data['jumlah_data'] = $post['post']['jumlah_data'];
        $data['jumlah_data_berlabel'] = $post['post']['jumlah_data_berlabel'];
        $data['k'] = $post['post']['k'];
        $data['kolom_display'] = $post['post']['kolom_display'];
        $data['kolom_kelas'] = $post['post']['kolom_kelas'];

        parent::update($data, "id = $id");
    }

    public function insert($post)
    {
        $path = INC_ROOT."/public/datasets/input/";
        $new_file_name = $post['files']['file']['name'];
        $new_file = $path . $new_file_name;
        move_uploaded_file($post['files']['file']['tmp_name'], $new_file);

        $data['nama_file'] = $new_file_name;
        $data['nama'] = $post['post']['nama'];
        $data['jumlah_data'] = $post['post']['jumlah_data'];
        $data['jumlah_data_berlabel'] = $post['post']['jumlah_data_berlabel'];
        $data['k'] = $post['post']['k'];
        $data['kolom_display'] = $post['post']['kolom_display'];
        $data['kolom_kelas'] = $post['post']['kolom_kelas'];

        parent::insert($data);
    }
}