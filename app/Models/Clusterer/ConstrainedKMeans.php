<?php

/**
 * Created by PhpStorm.
 * User: fathimtiaz
 * Date: 2/5/2017
 * Time: 9:44 AM
 */

namespace App\Models\Clusterer;

use App\Models\DatasetModel;

class ConstrainedKMeans extends KMeansBySeeding
{
    public function __construct(DatasetModel $dataset)
    {
        parent::__construct($dataset);
        $this->buildClusters();
    }

    private function buildClusters()
    {
        if ($this->dataset->query['k'] > count($this->dataset->data))
            return false;

        $this->initializeCentroids();
        do {
            $old_cluster_assignments = $this->clustersAssignments;
            $this->clustersAssignments = null;

            $this->assignLabeledInstancesToClusters();

            foreach($this->dataset->unlabeledData as $index => $data)
            {
                $this->clustersAssignments[$this->closestCentroid($index)][] = $index;
            }

            $this->iteration++;
            $this->repositionCentroids();

        } while($old_cluster_assignments != $this->clustersAssignments);

        $this->countClusters();
    }
}