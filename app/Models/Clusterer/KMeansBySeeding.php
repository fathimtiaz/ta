<?php

/**
 * Created by PhpStorm.
 * User: fathimtiaz
 * Date: 2/5/2017
 * Time: 9:44 AM
 */

namespace App\Models\Clusterer;

use App\Models\DatasetModel;

class KMeansBySeeding
{
    protected $dataset;

    protected $centroids;

    public $clustersAssignments;

    public $clustersCounts;

    public $iteration = 0;

    public function __construct(DatasetModel $dataset)
    {
        $this->dataset = $dataset;
    }

    public function initializeCentroids()
    {
        $this->assignLabeledInstancesToClusters();

        for($i = 0; $i < $this->dataset->query['k']; $i++)
        {
            $this->calculateCentroid($i);
        }
    }

    protected function assignLabeledInstancesToClusters()
    {
        foreach($this->dataset->labeledData as $index => $data)
        {
            $this->clustersAssignments[$data[$this->dataset->query['kolom_kelas']]][] = $index;
        }
    }

    protected function calculateCentroid($cluster_index)
    {
        $cluster_instances_values = array();
        foreach($this->clustersAssignments[$cluster_index] as $instance_index)
        {
            $cluster_instances_values[] = $this->dataset->data[$instance_index];
        }
        $cluster_instances_values = fn_transpose($cluster_instances_values);

        $new_centroid = array();

        foreach($cluster_instances_values as $value)
        {
            $new_centroid[] = array_sum($value) / count($value);
        }

        $this->centroids[$cluster_index] = $new_centroid;
    }

    protected function repositionCentroids()
    {
        for($i = 0; $i < $this->dataset->query['k']; $i++)
        {
            $this->calculateCentroid($i);
        }
    }

    protected function closestCentroid($instance_index)
    {
        $closest_centroid_index = null;
        $closest_distance = PHP_INT_MAX;
        foreach($this->centroids as $index => $centroid)
        {
            $distance = fn_euclidean_distance($this->dataset->data[$instance_index], $centroid);
            if ($distance < $closest_distance)
            {
                $closest_centroid_index = $index;
                $closest_distance = $distance;
            }
        }
        return $closest_centroid_index;
    }

    protected function countClusters()
    {
        foreach($this->clustersAssignments as $index => $cluster)
        {
            $this->clustersCounts[$index] = count($cluster);
        }
    }
}